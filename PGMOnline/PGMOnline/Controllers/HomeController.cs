﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PGMOnline.Models;

namespace PGMOnline.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Message = "This message will only display if Viewbag.Message works";
            ViewBag.Time = DateTime.Now.ToString("h:mm:ss tt");
            return View("PGM");
        }
        public IActionResult SupervisorInfo()
        {
            Supervisor supervisor1 = new Supervisor {Id= 1, Name = "Dean" };
            Supervisor supervisor2 = new Supervisor { Id = 2, Name = "Greg" };
            List<Supervisor> supervisors = new List<Supervisor>();
            supervisors.Add(supervisor1);
            supervisors.Add(supervisor2);
            return View(supervisors);
        }
    }
}
